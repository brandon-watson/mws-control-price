# -*- coding: utf-8 -*-
from datetime import datetime
from python_mws import *
import os
import boto3

"""
MWS Account Information
"""
ACCESS_KEY = "AKIAJEF5SOTB2Q4NBIKA"
SECRET_KEY = "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"
DEVELOPER_ID = "9109-9438-4906"
SELLER_ID = "A2QJWU608T6JR"
MARKETPLACE_ID = "ATVPDKIKX0DER"

"""
Inventory Reports
"""
# http://docs.developer.amazonservices.com/en_US/reports/Reports_ReportType.html
_Inventory_Report = "_GET_FLAT_FILE_OPEN_LISTINGS_DATA_"
_All_Listings_Report = "_GET_MERCHANT_LISTINGS_ALL_DATA_"
_Active_Listings_Report = "_GET_MERCHANT_LISTINGS_DATA_"
_Inactive_Listings_Report = "_GET_MERCHANT_LISTINGS_INACTIVE_DATA_"
_Open_Listings_Report = "_GET_MERCHANT_LISTINGS_DATA_BACK_COMPAT_"
_Open_Listings_Report_Lite = "_GET_MERCHANT_LISTINGS_DATA_LITE_"
_Open_Listings_Report_Liter = "_GET_MERCHANT_LISTINGS_DATA_LITER_"

"""
FeedType enumeration
"""
# http://docs.developer.amazonservices.com/en_US/feeds/Feeds_FeedType.html
_Product_Feed = "_POST_PRODUCT_DATA_"
_Inventory_Feed = "_POST_INVENTORY_AVAILABILITY_DATA_"
_Overrides_Feed = "_POST_PRODUCT_OVERRIDES_DATA_"
_Pricing_Feed = "_POST_PRODUCT_PRICING_DATA_"

print("starting function")


class Prices:
    def __init__(self):
        self.mws_report = Reports(access_key=ACCESS_KEY, secret_key=SECRET_KEY, account_id=SELLER_ID)
        self.mws_feed = Feeds(access_key=ACCESS_KEY, secret_key=SECRET_KEY, account_id=SELLER_ID)

        self._price_xml_content = """<?xml version="1.0" encoding="utf-8"?>
        <AmazonEnvelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:noNamespaceSchemaLocation="amzn-envelope.xsd">
        <Header>
        <DocumentVersion>1.01</DocumentVersion>
        <MerchantIdentifier>{}</MerchantIdentifier>
        </Header>
        <MessageType>Price</MessageType>
        {}</AmazonEnvelope>"""

        self._price_xml_msg = """<Message><MessageID>{}</MessageID>
        <Price><SKU>{}</SKU>
        <StandardPrice currency="USD">{}</StandardPrice>
        </Price>
        </Message>
        """
        self.active_list = []
        self.active_report_id = None
        self.field_list = None
        self.exclusion_list = []
        self.current_time = datetime.utcnow()
        self.ddb = boto3.resource('dynamodb')

    def get_block_asin(self):
        """
        Get exclusion ANSI from DynamoDB named "Exclusion"
        :return: 
        """
        table = self.ddb.Table('Exclusion')
        response = table.scan()
        if response['Count'] > 0:
            for item in response['Items']:
                self.exclusion_list.append(item['asin'])
        else:
            self.exclusion_list = []

    def is_past(self, dt_string):
        """
        Check if it is past time.
        :param dt_string: 
        :return: 
        """
        return (self.current_time - datetime.strptime(dt_string.replace(" ", "")[:-6], '%Y-%m-%dT%H:%M:%S')).days

    def set_active_list(self, report_id):
        """
        Set Active_list value from Report.
        :param report_list: 
        :return: 
        """
        self.active_list = []

        active_wrapper = self.mws_report.get_report(report_id=report_id)
        rows = (line.split('\t') for line in active_wrapper.parsed.splitlines())
        print('exclusion list: {}'.format(self.exclusion_list))
        for index, row in enumerate(rows):
            if index == 0:
                self.field_list = row
                continue

            if any(str(exc) in row[16] for exc in self.exclusion_list):
                continue
            self.active_list.append({self.field_list[index]: item for index, item in enumerate(row)})
        print('active_list: {}'.format(self.active_list))

    def make_feed_content(self, param=0.5):
        """
        make submit feed context as xml file.
        :param param: 
        :return: 
        """
        try:
            param = os.environ["price"]
        except:
            pass

        if len(self.active_list) == 0:
            raise ValueError("No active products here.")

        msg_content = ""
        for index, row in enumerate(self.active_list):
            msg_content += self._price_xml_msg.format(index+1, str(row['seller-sku']), str(float(row['price']) + float(param)))

        feed_content = self._price_xml_content.format(SELLER_ID, msg_content)
        print('feed content: {}'.format(feed_content))
        return feed_content

    def detect_price_list(self, report_list):
        """
        Get the price list from report list within a day.
        :return: 
        """
        for item in report_list:
            if self.is_past(item['AvailableDate']) <= 1 and item["ReportType"] == _Active_Listings_Report:
                self.active_report_id = item['ReportId']
                print(self.active_report_id)
                return True
        return False

    # main function.
    def check_active_list(self):
        """
        if report exists, then set active list directly, otherwise send report request again.
        :return: 
        """
        report_list = self.mws_report.get_report_list()

        # if report exists within a day, then use that report id directly.
        if self.detect_price_list(report_list.parsed["ReportInfo"]):
            self.set_active_list(self.active_report_id)

        # generate new report.
        else:
            print(self.mws_report.request_report(report_type=_Active_Listings_Report))
            self.check_active_list()

    def reprice_active_list(self, param=0.5):
        """
        Process to reprice the items in active product list.
        :param param: 
        :return: 
        """
        feed_content = self.make_feed_content()
        print(self.mws_feed.submit_feed(feed=feed_content, feed_type=_Pricing_Feed,
                                        marketplaceids=[MARKETPLACE_ID]))
        # print(self.mws_feed.get_feed_submission_result(feedid="83499017459"))


def lambda_handler(event, context):
    price = Prices()
    price.get_block_asin()
    price.check_active_list()
    price.reprice_active_list()
    price.reprice_active_list()
    return {"status": "Success"}